import axios from 'axios';

export interface Domain {
    domainId: string;
    name: string;
    description: string;
}

interface DomainResponse {
    results: Domain[]
    totalResults: number;
}


export const getDomains = async (accessToken: string) => {

    const res = await axios.get<DomainResponse>('https://api.data.cimpress.io/v0/domains', {
        headers: {
            authorization: `Bearer ${accessToken}`
        }
    })

    return res.data.results;
}