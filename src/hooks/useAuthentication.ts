import { useEffect, useState } from "react";

const { centralizedAuth } = require('@cimpress/simple-auth-wrapper');
const CLIENT_ID = '3Qw0jNwNY10eCWpL1LX3fRGHUxUfRnZP'

export interface Auth0Profile {
    given_name: string;
    name: string;
    email_verified: boolean;
    email: string;
    sub: string;
    'https://claims.cimpress.io/account': string;
    'https://claims.cimpress.io/canonical_id': string;
}

export const authWrapper = new centralizedAuth({
    clientID: CLIENT_ID,
    redirectRoute: '/',
    logoutRedirectUri: '/'
});


export interface AuthenticationProps {
    isAuthenticating: boolean;
    profile?: Auth0Profile;
    authenticationError?: Error;
    accessToken?: string;
    login(): void;
    logout(): void;
}


const login = async (setError: any, setIsAuthenticating: any, setProfile: any, setAccessToken: any) => {

    setError(undefined);
    setIsAuthenticating(true);
    try {
        const nextUri = window.location.pathname + window.location.search;
        await authWrapper.ensureAuthentication({ nextUri });
        const profile = authWrapper.getProfile() as Auth0Profile;
        const accessToken = authWrapper.getAccessToken();
        setProfile(profile);
        setAccessToken(accessToken);
    } catch (error) {
        setError(error);
    }
    setIsAuthenticating(false);
};

export const useAuthentication = (): AuthenticationProps => {

    const [isAuthenticating, setIsAuthenticating] = useState(false);
    const [accessToken, setAccessToken] = useState(undefined);
    const [error, setError] = useState(undefined);
    const [profile, setProfile] = useState(undefined as Auth0Profile | undefined);

    const logout = async () => {
        await authWrapper.logout('/');
    };


    useEffect(() => {
        login(setError, setIsAuthenticating, setProfile, setAccessToken);
    }, [accessToken]);

    return {
        isAuthenticating,
        profile,
        accessToken,
        authenticationError: error,
        login: () => login(setError, setIsAuthenticating, setProfile, setAccessToken),
        logout
    };
};
