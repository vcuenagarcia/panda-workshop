import React, { useEffect, useState } from "react";
import { Domain, getDomains } from "../clients/dataPortal";


const loadDomains = async (accessToken: string, setDomains: React.Dispatch<Domain[]>, setIsLoading: React.Dispatch<boolean>) => {
    console.log('Loading')
    setIsLoading(true);
    const domains = await getDomains(accessToken || '');
    setDomains(domains);
    setIsLoading(false);
  }
  
export const useDomains = (accessToken: string | undefined) => {

    const [domains, setDomains] = useState([] as Domain[]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
      if (accessToken) {
        loadDomains(accessToken, setDomains, setIsLoading);
      }
    }, [accessToken])

    return {
        domains,
        isLoading
    }
}