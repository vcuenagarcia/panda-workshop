import React from 'react';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Header from '@cimpress-technology/react-platform-header';
import { useAuthentication } from '../hooks/useAuthentication';


export const MyHeader: React.FC = () => {

    const { profile, login, logout, accessToken } = useAuthentication();

    return <Header
        appTitle={'Panda Workshop'}
        profile={profile}
        onLogInClicked={login}
        onLogOutClicked={logout}
        isLoggedIn={!!accessToken}
        accessToken={accessToken}
    />;
}
