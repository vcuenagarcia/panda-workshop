import React from 'react';
import { Card } from '@cimpress/react-components';


export interface McpDomainItemProps {
    domainName: string;
}

export const McpDomainItem: React.FC<McpDomainItemProps> = (props) => {
    return <div style={{
        marginBottom: 25
    }}>
        <Card>
            <h3>{props.domainName}</h3>
        </Card>
    </div>
}