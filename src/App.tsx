import React, { useState } from 'react';
import { TextField, shapes } from '@cimpress/react-components';
import { MyHeader } from './components/MyHeader';
import { McpDomainItem } from './components/McpDomainItem';
import './App.css';
import { useAuthentication } from './hooks/useAuthentication';
import { useDomains } from './hooks/useDomains';


function App() {

  const { accessToken } = useAuthentication();
  const [searchText, setSearchText] = useState('');
  const { domains, isLoading } = useDomains(accessToken);

  const domainItems = domains
    .filter(domain => domain.name.toLowerCase().includes(searchText.toLowerCase()))
    .map(domain => {
      return <McpDomainItem key={domain.domainId} domainName={domain.name} />
    })


  if (isLoading) {
    return <shapes.Spinner />
  }

  return (
    <div className="App">
      <MyHeader />
      <div className='App-Body'>
        <TextField
          label='Search'
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
        />
        {domainItems}
      </div>
    </div>
  );
}

export default App;
